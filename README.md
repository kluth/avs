Are you looking for the (functional) AVS models? If so, then please have a look at [this repository](https://bitbucket.org/kluth/sl-model/issues/1/interested-in-this-source-code) or [visit my homepage](https://www.techfak.de/~tkluth).

Thanks,
Thomas
